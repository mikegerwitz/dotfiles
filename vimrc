" vi: set tw=0
"
" Mike Gerwitz's personal vim configuration
"
" Copyright (C) 2013 Mike Gerwitz
"
"  This program is free software: you can redistribute it and/or modify
"  it under the terms of the GNU General Public License as published by
"  the Free Software Foundation, either version 3 of the License, or
"  (at your option) any later version.
"
"  This program is distributed in the hope that it will be useful,
"  but WITHOUT ANY WARRANTY; without even the implied warranty of
"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"  GNU General Public License for more details.
"
"  You should have received a copy of the GNU General Public License
"  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"
" Refactoring needed.
" "

" pathogen
call pathogen#incubate()

" general configuration options
set ai
set autochdir
set backspace=indent,eol,start
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set bufhidden=hide
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set et
set exrc
set fdm=marker
set history=9000
set hlsearch
set incsearch
set modeline
set number
set ruler
set sessionoptions=curdir,folds,globals,help,localoptions,options,resize,tabpages,winpos,winsize
set scrolloff=4
set spelllang=en_us
set spellfile=~/.vim/spellfile/common.add,~/.vim/spellfile/dev.add
set sw=4
set t_Co=256
set title
set ts=4
set tw=76
set undolevels=3000
set updatecount=50
set visualbell
syn on
color slatemg

" BASH-like tab completion
set wildmenu
set winminheight=0
set wildmode=longest,list,full

" enable filetype plugins
filetype on
filetype plugin on
filetype plugin indent on

" show whitespace characters
set list listchars=tab:>-,trail:•,precedes:•

" printing
set printfont=:h8
set printoptions=paper:letter,number:y,syntax:y,left:15pc
nmap <leader>ps :set printoptions+=duplex:short<CR>
nmap <leader>pS :set printoptions-=duplex:short<CR>

" abbreviations
iab __NAME Mike Gerwitz
iab __UN mikegerwitz
iab __EMAIL mike@mikegerwitz.com

" save a generic session in case of an oopsie
au BufWrite * silent :call WriteSessAuto()
au BufWrite * silent :call WriteSessAuto()

function! WriteSessAuto()
  " one capital letter for the Sessname global so that we can store it in the
  " session file
  let filename = '~/.vimsess-' .
    \ ( !exists( 'g:Sessname' ) || ( empty( g:Sessname ) )
        \ ? 'autosave' : (g:Sessname)
    \ )

  exec 'mksession! ' . filename
endfunction

" custom statusline
set statusline=%<%f\ %h%m%r%=[%n]\ \%-14.([%b:%B]\ \ %l,%c%V%)\ %P

" make the C-e and C-y mappings less painful
nnoremap <C-e> 3<C-e>
nnoremap <C-y> 3<C-y>
"
" redraw
nmap <silent> <leader>R :redraw!<CR>

" nohlsearch shortcut
nmap <silent> <leader>h :silent :nohlsearch<CR>

" for MultipleSearch
let g:MultipleSearchMaxColors = 10

" toggle cursorline/cursorcolumn
nmap <leader>c :set cursorline! cursorcolumn!<CR>
nmap <leader>C :set cursorcolumn!<CR>

" omnicomplete
set ofu=syntaxcomplete#Complete
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml,xsd,xslt set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete


" maps
""""""
" easy window switching
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_

" easy common alignments
map <C-A>= :Align =:set et
map <C-A>> :Align =>:set et
map <C-A>A :Align AS:set et


" focus fold
" map zO zMzo

" easy maximize window
map <C-W><SPACE> <C-W>_<C-W>k4<C-W>+<C-W>j
map <C-W><C-SPACE> <C-W><C><SPACE>

" toggle above fold
map zK zkza``
" toggle below field
map zJ zjza``

" comment out selected lines
map <C-A><C-D>c :s/^/\/\//
" remove comments
map <C-A><C-D>x :s/^\/\///

" CommandT
let g:CommandTMaxFiles=100000
map <silent> <leader>t :CommandT ~/gitrepos<CR>
map <silent> <leader>T :CommandTFlush<CR>:CommandT ~/gitrepos<CR>

map <silent> <leader>Hr :SearchReset<CR>:nohlsearch<CR>
map <silent> <leader>HR :SearchBuffersReset<CR>:nohlsearch<CR>
map <silent> <leader>vs :source ~/.vimrc<CR>
map <silent> <leader>gs :source ~/.vimrc<CR>:source ~/.gvimrc<CR>
map <silent> <leader>ss :setlocal spell spelllang=en_us<CR>
map <silent> <leader>S :setlocal nospell<CR>
map <silent> <leader>p :set paste<CR>
map <silent> <leader>P :set nopaste<CR>

" obnoxious mail ending chars (e.g. mutt)
map <silent> <leader>M :%s/=\n//g<CR>

" git blame history made easy
map <silent> <leader>gb :%!git blame -- %<CR>
map <silent> <leader>g< ^"byt :exe '%!git blame '.@b.'^ -- %'<CR>
map <silent> <leader>g. ^"byt :exe '%!git log -p '.@b.'^..'.@b<CR>

" snippets
function! ReloadSnippets( snippets_dir, ft )
    if strlen( a:ft ) == 0
        let filetype = "_"
    else
        let filetype = a:ft
    endif

    call ResetAllSnippets()
    call GetSnippets( a:snippets_dir, filetype )
endfunction

" reload snippets
nmap <leader>rs :call ReloadSnippets(snippets_dir, &filetype)<CR>

" move single line of nested XML onto multiple lines and indent
map <leader>xj ^f>a<CR><ESC>>>$F<i<CR><ESC><<k^
map <leader>xJ ^f/ldF A<CR><C-_><ESC>O<TAB>

" xmllint
map <leader>xl :%!xmllint --format -<CR>

" system-specific configuration
source ~/.vimrc-local
