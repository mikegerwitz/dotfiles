#!/bin/bash
# Executed by bash when login shell exits
#
# Copyright (C) 2013 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

# when leaving a console (taking into account nested bash sessions), clear the
# screen for privacy/security reasons; otherwise, previous command lines remain
# both visible and buffered
if [ "$SHLVL" -eq 1 ]; then
  [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi
