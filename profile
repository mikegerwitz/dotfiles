#!/bin/bash
# Executed on login (that is---a login shell)
#
# Copyright (C) 2013,2015 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Note that this script should include *only* what is specific to logins; all
# other configuration is handled by .bashrc.
##

# execute fbterm if logging on from a tty
if [ "$TERM" == linux ] && tty | grep -q /dev/tty; then
  shopt -s execfail
  which fbterm >/dev/null && exec fbterm
fi

# .bashrc is only sourced by bash on non-login shells (see bash(1)), so we need
# to do so ourselves
if [ -n "$BASH_VERSION" ]; then
  rcfile="$HOME/.bashrc"
  [ -f "$rcfile" ] && . "$rcfile"
fi
