;; Tangle configuration files and weave documentation
;;
;;  Copyright (C) 2015 Mike Gerwitz
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(package-initialize)

(require 'ox-publish)

;; FIXME: we're relying on the fact that we do not yet have a hierarchy
(defconst html-nav-bar
  "<nav id=\"nav-rel\">
  <ul>
    <li><a href=\"./\">Home</a></li>
  </ul>
</nav>")

(setq org-confirm-babel-evaluate nil
      org-src-fontify-natively   t

      org-publish-project-alist
      `(("config-html"
         :base-directory "src"
         :publishing-directory "www-root"
         :recursive t
         :base-extension "org$"
         :exclude "_inc"

         :headline-levels 5
         :section-numbers t

         :makeindex t

         :html-html5-fancy t
         :html-head-include-default-style nil
         :html-preamble ,html-nav-bar

         :publishing-function my/org-publish-tangle)))

(defun my/org-publish-tangle (plist filename pub-dir)
  (org-babel-tangle-file filename)
  (org-html-publish-to-html plist filename pub-dir))

(org-publish "config-html")
