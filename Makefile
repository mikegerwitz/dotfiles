#!/bin/sh
# Mike Gerwitz's personal configuration build
#
# Copyright (C) 2015 Mike Gerwitz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

.PHONY: default tangle-and-weave

default: tangle-and-weave

tangle-and-weave:
	emacs --batch -l project.el

install:
	stow -vd . -t "$(HOME)" --ignore='^_|.*\.(org|inc)$$' src

uninstall:
	stow -vd . -t "$(HOME)" -D src
