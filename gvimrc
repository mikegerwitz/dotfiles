" vi: set tw=0
"
" Mike Gerwitz's personal gvim configuration
"
" Copyright (C) 2013 Mike Gerwitz
"
"  This program is free software: you can redistribute it and/or modify
"  it under the terms of the GNU General Public License as published by
"  the Free Software Foundation, either version 3 of the License, or
"  (at your option) any later version.
"
"  This program is distributed in the hope that it will be useful,
"  but WITHOUT ANY WARRANTY; without even the implied warranty of
"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"  GNU General Public License for more details.
"
"  You should have received a copy of the GNU General Public License
"  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"
" Most configuration options are within ~/.vimrc---these are only the
" options that apply to the GTK+ software gvim.
" "

set guifont=Droid\ Sans\ Mono\ 8

" remove all those annoying GUI options that take up space
set guioptions=

" There is rarely a time where I use gvim and do not want to strip trailing
" whitespace or retab. If I do not want this, I do not use gvim.
autocmd BufWrite * :silent! :%s:\(\S\+\)\?\s\+$:\1:g
autocmd BufWrite * silent :retab
