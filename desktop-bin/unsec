#!/bin/bash
#
# Decrypt ~/.enc and run any hooks
#
# Copyright (C) 2013 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Prompts to decrypt the encrypted directory after first running a premount
# script. Should the script succeed, mounting proceeds, after which a
# postmount script is run. In the event that the latter fails, the mount
# point will remain mounted, so any abort operations must occur in premount.
##

avail="$HOME/.enc/.available"
premount="$HOME/.enc/.premount"
postmount="$HOME/.enc/.postmount"

# enc-setup creates .available within the encrypted directory; if it's
# available, then ~/.enc-data is already mounted
[ -e "$avail" ] && exit

# execute pre-mount script to allow system-specific preparation (note that
# this premount script exists within the mount point, meaning it'll be
# hidden as soon as the mount succeeds)
[ -x "$premount" ] && {
  "$premount" || {
    err=$?
    echo "fatal: $premount failed!" >&2
    exit $err
  }
}

# mount the directory understanding that ~/.enc is very likely non-empty (to
# provide ``secured'' defaults
encfs ~/.enc-data ~/.enc -ononempty \
  && {
    [ ! -x "$postmount" ] || "$postmount" || {
      err=$?
      echo "warning: mounted, but $postmount failed!"
      exit $err
    }
  } \
  || {
    err=$?
    echo "fatal: mount failed"
    exit $err
  }
